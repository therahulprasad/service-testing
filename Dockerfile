FROM ubuntu:bionic

WORKDIR /app
ADD run /app

CMD /app/run
